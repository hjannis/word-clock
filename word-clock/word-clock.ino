#include "Definitions.h"

#include "Colors.h"
#include "Pixel.h"

#include "TimeHandler.h"
#include "Test.h"
#include "Clock.h"

#define NUMPIXELS 114

#define SYNC_PERIODE 3600000
#define SYNC_PERIODE_SUNDOWN 3600000

Pixel pixel(NUMPIXELS);
TimeHandler timeHandler;

Clock letter_clock(&pixel);

/*--------------------------------------------*/
#ifdef ETHERNET_EDITION
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "Timer.h"
#include "RestServer.h"
#include "PongGame.h"
#include "Snake.h"
#include "DigiClock.h"

Timer timer(&pixel);
RestServer server(&pixel, &letter_clock, &timer);
PongGame pong(&pixel, &server);
Snake snake(&pixel, &server);
WiFiClient wifiClient;
WiFiClientSecure clientSecure;
HTTPClient sender;
DigiCLock digiClock(&pixel);

#ifdef WITH_GYRO
#include "BalanceBoard.h"
BalanceBoard balanceboard(&pixel, &server);
#endif

/* brightness sunset sunrise */
int sunset = 0;
int sunrise = 0;
long lastSynchSunset = 0L;
long lastSynchOffset = 0L;

#endif
/*--------------------------------------------*/

Test test(&pixel);

boolean firstStart = true;
boolean refresh = true;

long lastSync = 0L;
long lastDFCTime = 0L;

int timezone_offset_hour = 0;

int clock_brightness = 255;

void setup()
{

  Serial.begin(115200);
#ifdef DEBUG
  Serial.println("Start");
#endif
  pixel.begin();
#ifdef ETHERNET_EDITION
  server.begin();
#endif
  timeHandler.begin();
}

void backgroundSync()
{
  if (lastSync + SYNC_PERIODE < millis() || lastSync == 0)
  {
#ifdef WITH_DCF
    timeHandler.updateDCF();
    if (lastDFCTime != timeHandler.getDCFTime())
    {
      timeHandler.adjust();
      lastDFCTime = timeHandler.getDCFTime();
      lastSync = millis();
      timeHandler.log();
    }
#else if defined(ETHERNET_EDITION)
    timeHandler.updateNTPTime();
    lastSync = millis();
    timeHandler.log();
#endif
  }
}

void loop()
{
  boolean man_refresh = false;
  backgroundSync();
#if defined(ETHERNET_EDITION)
  server.handleBackground();
  switch (server.getMode())
  {
  case 0:
  case 6: // default clock
  {
#if defined(WITH_RTC)
    unsigned long internTime = timeHandler.getRTCTime();
#else
    unsigned long internTime = timeHandler.getInternTime();
#endif
    man_refresh = server.manual_refresh_display;
#endif
    if (!refresh && internTime % 60 != 0)
      refresh = true;
    if ((internTime % 60 == 0 && refresh) || firstStart || man_refresh)
    {
      refresh = false;
      firstStart = false;
      server.manual_refresh_display = false;
      getTimeOffset();
#ifdef DEBUG
      Serial.println("offset: " + String(timezone_offset_hour));
#endif
      internTime += (3600 * timezone_offset_hour);
#if defined(WITH_BRIGHTNESS_SENSOR)
      pixel.getPixels()->setBrightness(map(analogRead(BRIGHTNESS_SENSOR_PIN), 0, 1024, 0,255);
#elif defined(ETHERNET_EDITION)
    getSundownBrightness();
    calculateBrightness(internTime);
    pixel.getPixels()
        ->setBrightness(clock_brightness);
#else
    pixel.getPixels()
        ->setBrightness(255);
#endif
      if(server.getMode() == 0){
        letter_clock.timeToPixel(internTime);
      }
      else if(server.getMode() == 6) {
        digiClock.time(internTime);}
    }
#ifdef ETHERNET_EDITION
    break;
  }
  case 1: // round clock

    break;
  case 2: // pong-game
    test.circle();
    pong.reset();
    pong.play();
    break;
  case 3: // snake
    test.circle();
    snake.reset();
    snake.play();
    break;
  case 4: // timer
    timer.run();
    break;
  case 5:
    balanceboard.start();
    break;
  case 0xff: // freestyle
    pixel.update();
    break;
  }
#endif
}

#ifndef WITH_BRIGHTNESS_SENSOR
#ifdef ETHERNET_EDITION
void getSundownBrightness()
{
#ifdef DEBUG
  Serial.print("sundown start\t");
#endif
  if (lastSynchSunset + SYNC_PERIODE_SUNDOWN < millis() || lastSynchSunset == 0)
  {
#ifdef DEBUG
    Serial.print("sundown synch\t");
#endif
    if (sender.begin(wifiClient, "http://api.sunrise-sunset.org/json?lat=36.7201600&lng=-4.4203400")) // for frankfurt main
    {
      int httpCode = sender.GET();
#ifdef DEBUG
      Serial.print("sundown client alive\t" + String(httpCode));
#endif
      if (httpCode > 0)
      {
        if (httpCode == 200)
        {
          String resp = sender.getString();
          int i = resp.indexOf("sunrise");
          String s_sunrise = resp.substring(i + 10, resp.indexOf(":", i + 11));
          i = resp.indexOf("sunset", i + 11);
          String s_sunset = resp.substring(i + 9, resp.indexOf(":", i + 10));
          sunrise = timezone_offset_hour + s_sunrise.toInt() - 2; // -1/ +1: expands the fully light power of the clock about 2 hours
          sunset = timezone_offset_hour + s_sunset.toInt() + 1 + 12;
#ifdef DEBUG
          Serial.println("rise: " + String(sunrise));
          Serial.println("set: " + String(sunset));
#endif
          lastSynchSunset = millis();
        }
      }
      sender.end();
    }
  }
}

void calculateBrightness(unsigned long current_time)
{
  clock_brightness = server.user_brightness;
  if (server.manual_brightness)
  {
    if (server.time_set_brightness + 1200000 /*20min*/ < millis())
    {
      server.manual_brightness = false;
    }
#ifdef DEBUG
    Serial.println("server bright: " + String(clock_brightness));
#endif
  }
  else
  {
    int hour = numberOfHours(current_time);
    int minute = numberOfMinutes(current_time);
#ifdef DEBUG
    Serial.println("sun: " + String(lastSynchSunset) + ", " + String(sunrise) + ", " + String(sunset) + ", " + String(hour) + ", " + String(minute));
#endif

    if (hour == sunrise)
    {
      clock_brightness = (int)(((double)server.user_brightness / 60.0) * (minute)) + 10;
    }
    else if (hour == sunset)
    {
      clock_brightness = (int)(server.user_brightness * (1 - (((double)minute / 60.0)))) + 10;
    }
    else if (hour < sunrise || hour > sunset)
    {
      // nightmode
      clock_brightness = 10;
    }
  }
  //#ifdef DEBUG
  Serial.println("brightness: " + String(clock_brightness));
  //#endif
}
#endif
#endif

#ifdef ETHERNET_EDITION
#ifndef WITH_DCF
void getTimeOffset()
{
  if (lastSynchOffset + SYNC_PERIODE_SUNDOWN < millis() || lastSynchOffset == 0)
  {
    String api_call = String("https://api.ipgeolocation.io/timezone?apiKey=") + String(API_KEY_IPGEOLOCATION) + String("&location=Frankfurt/Germany");
    clientSecure.setInsecure();
    if (sender.begin(clientSecure, api_call)) //SHA1 Fingerprint of server cert
    {
      int httpCode = sender.GET();
      if (httpCode == 200)
      {
        String resp = sender.getString();
        int position_value_delimiter_time_offset_dst = resp.indexOf(":", resp.indexOf("timezone_offset_with_dst"));
        String s_offset = resp.substring(position_value_delimiter_time_offset_dst + 1, resp.indexOf(",", position_value_delimiter_time_offset_dst));
        timezone_offset_hour = s_offset.toInt();
        lastSynchOffset = millis();
      }
      sender.end();
    }
  }
}
#endif
#endif
