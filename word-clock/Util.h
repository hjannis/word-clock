#ifndef UTIL_H
#define UTIL_H

#include "Definitions.h"

//#include "Base32.h"

static const String hexChars = "0123456789abcdef";

// typedef const String (*encoderFunc)(byte b);

static const int pixel_matrix[10][11] = {
    {109, 108, 107, 106, 105, 104, 103, 102, 101, 100, 99},
    {88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98},
    {87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77},
    {66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76},
    {65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55},
    {44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54},
    {43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33},
    {22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32},
    {21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11},
    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}};

class Util
{
    /*
public:
    static byte *createRandByteArray(int size)
    {
        byte *pt = new byte[size];
        for (int i = 0; i < size; i++)
        {
            pt[i] = random(0, 256);
        }
        return pt;
    }

    /*
    static String bytesToB32(byte *bytes, int length)
    {
        return bytesEncode(bytes, length, [](byte b)
                           { return Base32::encode((int)b).c_str(); });
    }
    */
    /*
     static String bytesToHex(byte *bytes, int length)
     {
         return bytesEncode(bytes, length, [](byte b)
                            {
                                String tmp;
                                tmp += hexChars.charAt((b >> 4) & 0x0f);
                                tmp += hexChars.charAt(b & 0x0f);
                                return tmp;
                            });
     }

     // TODO the bytewise conversion is very inperformant!
     static String bytesEncode(byte *bytes, int length, encoderFunc encoder)
     {
         String hex = "";
         for (int i = 0; i < length; i++)
         {
             hex += encoder(bytes[i]);
         }
         return hex;
     }
     */
};
#endif // UTIL_H