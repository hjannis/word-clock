#ifndef GENERIC_SMARTHOME_DEVICE
#define GENERIC_SMARTHOME_DEVICE

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>

#define DEVICE_NAME "MySmartWordclockDevice"
#define INITIAL_AP_PASSWORD "12345678"
#define ANNOUNCEMENT_INTERVAL 60000

#define VERSION "v0.3.6"

class GenericSmartHomeDevice {
public:
  GenericSmartHomeDevice(int port)
    : server(port) {}
  void begin() {
    WiFi.disconnect();
    EEPROM.begin(512);  // Initializing EEPROM

    String esid;
    for (int i = 0; i < 128; i++) {
      char c = char(EEPROM.read(i));
      if (c == 0) break;
      esid += c;
    }

    String epass = "";
    for (int i = 128; i < 384; i++) {
      char c = char(EEPROM.read(i));
      if (c == 0) break;
      epass += c;
    }

    WiFi.begin(esid.c_str(), epass.c_str());
    #ifdef DEBUG
      Serial.println(esid + " : " + epass);
#endif
    if (testWifi()) {
      is_init = true;
#ifdef DEBUG
      Serial.print(WiFi.localIP());
#endif
    } else {
      /********* setup intit ap wifi for configuring device ***********/

      WiFi.mode(WIFI_AP);
      WiFi.softAPConfig(IPAddress(192, 168, 1, 1), IPAddress(192, 168, 1, 1), IPAddress(255, 255, 255, 0));
      WiFi.softAP(DEVICE_NAME, INITIAL_AP_PASSWORD);

      delay(10);
      setupConfigServer();
    }
    server.onNotFound([&]() {
      server.send(404, "text / plain", "404: Not found");
    });
    server.begin();
#ifdef DEBUG
    Serial.println("HTTP server started");
#endif
  }

  void setInterface(String interface_string) {
    interface = interface_string;
  }

  void serverBackgroundJob() {
    server.handleClient();
    if (is_init && lastTimeSynch + ANNOUNCEMENT_INTERVAL <= millis()) {
      publishDeviceBroadcast();
      lastTimeSynch = millis();
    }
  }

  ESP8266WebServer *getServer() {
    return &server;
  }

private:
  ESP8266WebServer server;
  WiFiUDP udp;
  const char *name;

  String index_html =
    "<!DOCTYPE HTML><html><head>\n"
    "<title>ESP Input Form</title>\n"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
    "</head><body>\n"
    "<form action=\"/config\">\n"
    "<input type=\"text\" name=\"ssid\" />\n"
    "<input type=\"password\" name=\"pw\" />\n"
    "<input type=\"submit\" value=\"submit\" />\n"
    "</form>\n"
    "</body></html>";

  String interface = "wordclock";

  long long lastTimeSynch = 0L;
  boolean is_init = false;

  void setupConfigServer() {
    server.on("/version", HTTP_GET, [&]() {
      server.send(200, "text/html", VERSION);
    });

    server.on("/", HTTP_GET, [&]() {
      server.send(200, "text/html", index_html);
    });

    server.on("/config", [&]() {
      String qsid = server.arg("ssid");
      String qpass = server.arg("pw");
      // String name = server.arg("name");
      if (qsid.length() > 0 && qpass.length() > 0) {
        ok();
        writeWifiParamToEEPROM(qsid, qpass);
        delay(10);

        /********* setup wifi connection to router ***********/
        WiFi.begin(qsid.c_str(), qpass.c_str());
#ifdef DEBUG
        Serial.println("We are now preparing the device for you!");
#endif
        if (testWifi()) {
          udp.begin(8181);
          is_init = true;
#ifdef DEBUG
          Serial.print(WiFi.localIP());
#endif
          publishDeviceBroadcast();
        } else {
          error("wifi");
        }
      } else {
        error();
      }
    });
  }

  void send(int code, String msg) {
    server.send(code, "text / plain", msg);
  }

  void send(String msg) {
    send(200, msg);
  }

  void error() {
    error("error");
  }

  void error(String msg) {
    send(404, msg);
  }

  void ok() {
    send("ok");
  }

  inline IPAddress calculateBroadcastAddress() {
    IPAddress adr = IPAddress((uint32_t)WiFi.localIP() | (~(uint32_t)WiFi.subnetMask()));
#ifdef DEBUG
    Serial.println(adr);
#endif
    return adr;
  }

  void publishDeviceBroadcast() {
#ifdef DEBUG
    Serial.println("broadcast");
#endif
    udp.beginPacket(calculateBroadcastAddress(), 38081);
    udp.write((String(ESP.getChipId(), HEX)).c_str());
    udp.endPacket();
  }

  bool testWifi(void) {
    int c = 0;
    while (c < 50) {
      if (WiFi.status() == WL_CONNECTED) {
        return true;
      }
      delay(500);
#ifdef DEBUG
      Serial.print('.');
#endif
      c++;
      yield();
    }
    return false;
  }

  void writeWifiParamToEEPROM(String qsid, String qpass) {
    for (int i = 0; i < 384; ++i) {
      EEPROM.write(i, 0);
    }
#ifdef DEBUG
    Serial.println(qsid);
    Serial.println("");
    Serial.println(qpass);
    Serial.println("");
#endif

    //        Serial.println("writing eeprom ssid:");
    for (int i = 0; i < qsid.length(); ++i) {
      EEPROM.write(i, qsid[i]);
#ifdef DEBUG
      Serial.print("Wrote: ");
      Serial.println(qsid[i]);
#endif
    }
    // Serial.println("writing eeprom pass:");
    for (int i = 0; i < qpass.length(); ++i) {
      EEPROM.write(128 + i, qpass[i]);
#ifdef DEBUG
      Serial.print("Wrote: ");
      Serial.println(qpass[i]);
#endif
    }
    EEPROM.commit();
  }
};

#endif  // GENERIC_SMARTHOME_DEVICE
