#ifndef TIMEHANDLER_H
#define TIMEHANDLER_H

#ifdef ETHERNET_EDITION
#include <NTPClient.h>
#include <WiFiUdp.h>
#endif

#include <RTClib.h>
#ifdef WITH_DCF
#include "MyDCF77.h"
#endif
#include <time.h>

class TimeHandler
{
public:
#ifdef ETHERNET_EDITION
    TimeHandler() : timeClient(ntpUDP, "pool.ntp.org")
    {
    }
#else
    TimeHandler()
    {
    }
#endif

    void begin()
    {
#ifdef ETHERNET_EDITION
        timeClient.begin();
#endif
#ifdef WITH_DCF
        dcf.init();
#endif
#ifdef WITH_RTC
        rtc.begin();
#endif
    }

    void adjust()
    {
        offset = millis();
#ifdef WITH_RTC
#ifdef WITH_DCF
        rtc.adjust(getDCFTime());
#else
        rtc.adjust(getNTPTime());
#endif
#endif
    }

#ifdef ETHERNET_EDITION
    void updateNTPTime()
    {
        timeClient.update();
        unsigned long now = timeClient.getEpochTime();
        last_synch = millis();
        offset = now;
    }
#endif

    unsigned long getInternTime()
    {
#ifdef WITH_DFC
        return getDCFTime() + (millis() - offset) / 1000L;
#else if defined(ETHERNET_EDITION)
        return offset + ((millis() - last_synch) / 1000L);
#endif
    }

#ifdef WITH_DCF
    long getDCFTime()
    {
        return dcf.getUnix();
    }

    int updateDCF()
    {
        return dcf.update();
    }

    int dcfMin()
    {
        return dcf.getMin();
    }

    int dcfSec()
    {
        return dcf.getSecond();
    }

    int dcfHour()
    {
        return dcf.getHour();
    }
#endif

#ifdef WITH_RTC
    const unsigned long getRTCTime()
    {
        return rtc.now().unixtime();
    }
#endif

    void log()
    {
#ifdef ETHERNET_EDITION
        Serial.println("NTP: " + String(getInternTime()));
#endif
#ifdef WITH_RTC
        Serial.println("DCF: " + String(dcfHour()) + ":" + String(dcfMin()) + ":" + String(dcfSec()) + " DCF unix: " + String(getDCFTime()) + ", RTC: " + String(getRTCTime()));
#endif
#ifdef WITH_DCF
        Serial.println("DCF unix: " + String(getDCFTime()) + " intern: " + String(getInternTime()));
#endif
    }

private:
#ifdef ETHERNET_EDITION
    WiFiUDP ntpUDP;
    NTPClient timeClient;
#endif
#ifdef WITH_RTC
    RTC_DS3231 rtc;
#endif
#ifdef WITH_DCF
    MyDCF77 dcf;
#endif
    unsigned long offset = 0L;
    unsigned long last_synch = 0L;
};

#endif // TIMEHANDLER_H