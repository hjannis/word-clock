#ifndef GYRO_H
#define GYRO_H

#include <Wire.h>

#define MPU_addr 0x68
#define minVal 265
#define maxVal 402

class Gyro
{
public:
    void begin()
    {
        Wire.begin();
        Wire.beginTransmission(MPU_addr);
        Wire.write(0x6B);
        Wire.write(0);
        Wire.endTransmission(true);
    }

    void stop()
    {
        Wire.endTransmission(true);
    }

    void getAbsolutAngle(double *values)
    {
        readValues();
        int xAng = map(AcX, minVal, maxVal, -90, 90);
        int yAng = map(AcY, minVal, maxVal, -90, 90);
        int zAng = map(AcZ, minVal, maxVal, -90, 90);

        x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
        y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
        z = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
        values[0] = x;
        values[1] = y;
        values[2] = z;
    }

    void getAcceleration(int16_t *values)
    {
        readValues();
        values[0] = AcX;
        values[1] = AcY;
        values[2] = AcZ;
    }

    void getGyroscopeVelocity(int16_t *values)
    {
        readValues();
        values[0] = GyX;
        values[1] = GyY;
        values[2] = GyZ;
    }

private:
    int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;

    double x;
    double y;
    double z;

    void readValues()
    {
        Wire.beginTransmission(MPU_addr);
        Wire.write(0x3B);
        Wire.endTransmission(false);
        Wire.requestFrom(MPU_addr, 14, true);
        AcX = Wire.read() << 8 | Wire.read();
        AcY = Wire.read() << 8 | Wire.read();
        AcZ = Wire.read() << 8 | Wire.read();
        GyX = Wire.read() << 8 | Wire.read();
        GyY = Wire.read() << 8 | Wire.read();
        GyZ = Wire.read() << 8 | Wire.read();
    }
};

#endif // GYRO_H