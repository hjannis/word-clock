#ifndef TEST_H
#define TEST_H

#include "Definitions.h"

#include "Pixel.h"
#include "Colors.h"
#include "Util.h"

class Test
{
public:
    Test(Pixel *pixel)
    {
        this->pixel = pixel;
    }

#ifdef DEBUG
    void all()
    {
        pixel->setAllPixels(Colors::RED);
        pixel->update();
        delay(20);
        pixel->setAllPixels(Colors::GREEN);
        pixel->update();
        delay(20);
        pixel->setAllPixels(Colors::BLUE);
        pixel->update();
        delay(20);
    }
#endif

    void hello()
    {
        pixel->setAllPixels(Colors::BLACK);
        pixel->setPixel(pixel_matrix[1][2], Colors::WHITE);  // H
        pixel->setPixel(pixel_matrix[1][6], Colors::WHITE);  // A
        pixel->setPixel(pixel_matrix[2][10], Colors::WHITE); // L
        pixel->setPixel(pixel_matrix[4][6], Colors::WHITE);  // L
        pixel->setPixel(pixel_matrix[3][1], Colors::WHITE);  // O
        pixel->update();
    }

    void circle()
    {
#ifdef DEBUG
        pixel->getPixels()->setPixelColor(pixel_matrix[5][5], Colors::WHITE);
        pixel->getPixels()->show();
        delay(100);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][5], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][5], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][6], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][5], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][6], Colors::WHITE);
        pixel->getPixels()->show();
        delay(100);

        pixel->getPixels()->setPixelColor(pixel_matrix[5][5], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][6], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][5], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][6], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[4][5], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][4], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][6], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][5], Colors::WHITE);
        pixel->getPixels()->show();
        delay(100);

        pixel->getPixels()->setPixelColor(pixel_matrix[4][5], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][4], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][6], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][5], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[4][5], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[4][6], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][7], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][7], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[7][5], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[7][6], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][4], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][4], Colors::WHITE);
        pixel->getPixels()->show();
        delay(100);

        pixel->getPixels()->setPixelColor(pixel_matrix[4][5], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[4][6], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][7], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][7], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[7][5], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[7][6], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][4], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][4], Colors::BLACK);

        pixel->getPixels()->setPixelColor(pixel_matrix[3][4], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[3][5], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[3][6], Colors::WHITE);

        pixel->getPixels()->setPixelColor(pixel_matrix[4][3], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[4][7], Colors::WHITE);

        pixel->getPixels()->setPixelColor(pixel_matrix[5][3], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][7], Colors::WHITE);

        pixel->getPixels()->setPixelColor(pixel_matrix[6][3], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][7], Colors::WHITE);

        pixel->getPixels()->setPixelColor(pixel_matrix[7][4], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[7][5], Colors::WHITE);
        pixel->getPixels()->setPixelColor(pixel_matrix[7][6], Colors::WHITE);
        pixel->getPixels()->show();
        delay(100);

        pixel->getPixels()->setPixelColor(pixel_matrix[3][4], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[3][5], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[3][6], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[4][3], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[4][7], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][3], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[5][7], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][3], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[6][7], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[7][4], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[7][5], Colors::BLACK);
        pixel->getPixels()->setPixelColor(pixel_matrix[7][6], Colors::BLACK);
        pixel->getPixels()->show();
        delay(200);
#endif
    }

    Pixel *pixel;
};
#endif // TEST_H