#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include "Arduino.h"
#include "Colors.h"

 #define DEBUG 1

// #define WITH_BRIGHTNESS_SENSOR

#define ETHERNET_EDITION

// #define WITH_RTC

// #define WITH_DCF

#define WITH_GYRO

#ifdef WITH_BRIGHTNESS_SENSOR
#define BRIGHTNESS_SENSOR_PIN A0
#endif

#ifdef WITH_DCF
#define DCF_PIN 6
#endif

#ifdef ETHERNET_EDITION
#define SERVER_PORT 80
#endif

#ifdef WITH_GYRO
#define GYRO_PIN 6
#endif

#define PIXEL_PIN 5

#define API_KEY_IPGEOLOCATION "YOUR-API-KEY"

//#define RGB

uint32_t pixel_color = Colors::WHITE;

#endif // DEFINITIONS_H
