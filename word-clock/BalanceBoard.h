#ifndef BALANCEBOARD_H
#define BALANCEBOARD_H

#include "Definitions.h"

#ifdef WITH_GYRO

#include "Pixel.h"
#include "RestServer.h"
#include "Colors.h"
#include "Gyro.h"
#include "Util.h"

#define UPDATE_RATE 50

class BalanceBoard
{
public:
    BalanceBoard(Pixel *pixel, RestServer *server) : pixel(pixel), server(server)
    {
        gyro = Gyro();
    }
    void start()
    {
        gyro.begin();
        while (true)
        {
            server->handleBackground();
            if (lasttime + UPDATE_RATE < millis())
            {
                if (server->action == 0)
                {
                    // exit
                    break;
                }
                pixel->setAllPixels(Colors::BLACK);
                if (ball_pos[0] >= 4.5 && ball_pos[1] >= 4.5 && ball_pos[0] <= 5.5 && ball_pos[1] <= 4.5)
                    pixel->setPixel(pixel_matrix[(int)ball_pos[0]][(int)ball_pos[1]], Colors::GREEN);
                else
                    pixel->setPixel(pixel_matrix[(int)ball_pos[0]][(int)ball_pos[1]], Colors::BLUE);
                pixel->update();
                double angles[3];
                gyro.getAbsolutAngle(angles);
                ball_pos[0] += 5.0 * (90.0 / angles[0]);
                ball_pos[1] += 5.0 * (90.0 / angles[1]);
                lasttime = millis();
            }
        }
    }

    void end()
    {
        gyro.stop();
    }

private:
    Pixel *pixel;
    RestServer *server;
    Gyro gyro;

    long lasttime = 0L;

    double ball_pos[2] = {5.0, 5.0};

    byte state = 0;
};

#endif // WITH_GYRO
#endif // BALANCEBOARD_H