#ifndef TIMER_H
#define TIMER_H

#include "Pixel.h"
#include "Colors.h"

class Timer
{
public:
    Timer(Pixel *pixel)
    {
        this->pixel = pixel;
    }

    void setDuration(long duration)
    {
        this->duration = duration;
    }

    void start()
    {
        running = true;
        start_time = millis();
        remaining = duration;
        pixel->setAllPixels(Colors::GREEN);
    }

    void run()
    {
        if (running)
        {
            if (remaining <= 0L)
            {
                if (last_blink + 1000 < millis())
                {
                    pixel->setAllPixels(leds_on ? Colors::BLACK : Colors::RED);
                    leds_on ^= true;
                    last_blink = millis();
                    pixel->update();
                }
            }
            else if (last_blink + 10 < millis())
            {
                int actualPixel = 1 + 108 * (1.0 - ((double)remaining / duration));
                pixel->setPixel(actualPixel, Colors::WHITE);
                pixel->setPixelRange(0, actualPixel - 1, Colors::BLACK);
                last_blink = millis();
                remaining = duration - (last_blink - start_time);
                pixel->update();
            }
        }
    }

    void stop()
    {
        running = false;
        remaining = 0L;
        last_blink = 0L;
        start_time = 0L;
        leds_on = false;
        pixel->setAllPixels(Colors::BLACK);
    }

private:
    Pixel *pixel;
    long duration = 0L;
    long remaining = 0L;
    long last_blink = 0L;
    long start_time = 0L;
    boolean running = false;
    boolean leds_on = false;
};

#endif // TIMER_H