#ifndef CLOCK_H
#define CLOCK_H

#include "Definitions.h"

#include "Pixel.h"
#include "Colors.h"

/* Useful Constants */
#define SECS_PER_MIN (60UL)
#define SECS_PER_HOUR (3600UL)
#define SECS_PER_DAY (SECS_PER_HOUR * 24L)

/* Useful Macros for getting elapsed time */
#define numberOfSeconds(_time_) (_time_ % SECS_PER_MIN)
#define numberOfMinutes(_time_) ((_time_ / SECS_PER_MIN) % SECS_PER_MIN)
#define numberOfHours(_time_) ((_time_ % SECS_PER_DAY) / SECS_PER_HOUR)
#define elapsedDays(_time_) (_time_ / SECS_PER_DAY)

struct letters
{
    letters(const int start, const int length) : start(start), length(length)
    {
    }
    const int start;
    const int length;
};

class Clock
{
public:
    void setConnecting(){
        pixel->setAllPixels(Colors::BLACK);
        funk();
        pixel->update();
    }

    Clock(Pixel *pixel)
    {
        this->pixel = pixel;
    }

    void timeToPixel(long time)
    {
        int hour = numberOfHours(time) % 12;
        int minute = numberOfMinutes(time);
        timeToPixel(hour, minute);
    }

    void timeToPixel(int hour, int minute)
    {
#ifdef DEBUG
        Serial.println("Time: " + String(hour) + ":" + String(minute));
#endif
        pixel->setAllPixels(Colors::BLACK);

        esist();

        int min = minute % 5;
        if (min != 0)
        {
            for (int i = 0; i < min; i++)
            {
                pixel->setPixel(110 + i, pixel_color);
            }
        }

        minute -= min;
        int mul = minute / 5;
        switch (mul)
        {
        case 0:
            uhr();
            break;
        case 1:
        case 2:
        case 3:
        case 4:
            pixel->setPixelRange(minute_map[mul - 1].start, minute_map[mul - 1].length, pixel_color);
            nach();
            break;
        case 5:
            pixel->setPixelRange(minute_map[0].start, minute_map[0].length, pixel_color);
            vor();
            halb();
            hour += 1;
            hour %= 12;
            break;
        case 6:
            halb();
            hour += 1;
            hour %= 12;
            break;
        case 7:
            pixel->setPixelRange(minute_map[0].start, minute_map[0].length, pixel_color);
            nach();
            halb();
            hour += 1;
            hour %= 12;
            break;
        case 8:
        case 9:
        case 10:
        case 11:
            pixel->setPixelRange(minute_map[12 - mul - 1].start, minute_map[12 - mul - 1].length, pixel_color);
            vor();
            hour += 1;
            hour %= 12;
            break;
        default:
            break;
        }

        if (hour == 1 && mul == 0)
        { //es ist ein(s) uhr
            pixel->setPixelRange(hour_map[0].start, hour_map[0].length - 1, pixel_color);
        }
        else
        {
            pixel->setPixelRange(hour_map[(hour + 11) % 12].start, hour_map[(hour + 11) % 12].length, pixel_color);
        }
        pixel->update();
    }

private:
    Pixel *pixel;
    letters hour_map[12] = {
        letters(44, 4),
        letters(51, 4),
        letters(40, 4),
        letters(33, 4),
        letters(55, 4),
        letters(22, 5),
        letters(16, 6),
        letters(29, 4),
        letters(3, 4),
        letters(0, 4),
        letters(58, 3),
        letters(11, 5)};

    letters minute_map[4] = {
        letters(99, 4),
        letters(88, 4),
        letters(77, 7),
        letters(92, 7)};

    void esist()
    {
        pixel->setPixelRange(108, 2, pixel_color);
        pixel->setPixelRange(104, 3, pixel_color);
    }

    void vor()
    {
        pixel->setPixelRange(66, 3, pixel_color);
    }

    void nach()
    {
        pixel->setPixelRange(73, 4, pixel_color);
    }

    void halb()
    {
        pixel->setPixelRange(62, 4, pixel_color);
    }

    void funk()
    {
        pixel->setPixelRange(69, 4, pixel_color);
    }

    void uhr()
    {
        pixel->setPixelRange(8, 3, pixel_color);
    }
};

#endif // CLOCK_H
