#ifndef PIXEL_H
#define PIXEL_H

#include "Definitions.h"

#include <Adafruit_NeoPixel.h>
#include "Colors.h"

class Pixel
{

public:
#ifdef RGB
    Pixel(int led_count) : pixels(led_count, PIXEL_PIN, NEO_GRB + NEO_KHZ800)
#else
    Pixel(int led_count) : pixels(led_count, PIXEL_PIN, NEO_GRBW + NEO_KHZ800)
#endif
    {
        this->led_count = led_count;
        pixels.begin();
#ifdef DEBUG
        pixels.setPixelColor(0, Colors::BLUE);
        pixels.show();
        delay(20);
        pixels.setPixelColor(0, Colors::BLACK);
        pixels.show();
#endif
    }

    String getState()
    {
        String state = "";

        return state;
    }

    void begin()
    {
        pixels.begin();
    }

    int getLedCount()
    {
        return this->led_count;
    }

    void setPixel(String pixel_nb, String pixel_color)
    {
        if (pixel_nb)
        {
            setPixel(pixel_nb.toInt(), pixel_color ? pixel_color.toInt() : Colors::WHITE);
        }
        else
            setAllPixels(pixel_color ? pixel_color.toInt() : Colors::WHITE);
    }

    void setPixel(int pixel_nb, uint32_t pixel_color)
    {
        pixels.setPixelColor(pixel_nb, pixel_color);
    }

    void setPixelRange(int pixel_nb, int count, uint32_t pixel_color)
    {
        for (int i = pixel_nb; i < pixel_nb + count; i++)
        {
            setPixel(i, pixel_color);
        }
    }

    void setAllPixels(uint32_t pixel_color)
    {
        pixels.fill(pixel_color);
    }

    void update()
    {
        pixels.show();
    }

    Adafruit_NeoPixel *getPixels()
    {
        return &pixels;
    }

private:
    int led_count;
    Adafruit_NeoPixel pixels;
};
#endif // PIXEL_H
