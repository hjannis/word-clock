#ifndef BASE32_H
#define BASE32_H

#include "Definitions.h"

#include <iostream>
#include <math.h>

class Base32
{

public:
    static std::string encode(int number)
    {
        std::string result = "";
        bool negative = false;
        if (number < 0)
        {
            negative = true;
        }

        number = abs(number);
        do
        {
            result = Base32::dict[fmod(floor(number), 32)] + result;
            number /= 32;
        } while (number > 0);

        if (negative)
        {
            result = "-" + result;
        }

        return result;
    }

    static int decode(std::string str)
    {
        int result = 0;
        int negative = 1;
        if (str.rfind("-", 0) == 0)
        {
            negative = -1;
            str = str.substr(1);
        }

        for (char &letter : str)
        {
            result += Base32::dict.find(letter);
            result *= 32;
        }

        return result / 32 * negative;
    }

private:
    static std::string dict;
};
#endif // BASE32_H