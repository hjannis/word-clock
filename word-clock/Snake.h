#ifndef SNAKE_H
#define SNAKE_HSNAKE_H

#include "Pixel.h"
#include "RestServer.h"
#include "Colors.h"
#include "Util.h"
#include "Test.h"

// TODO richtig machen

class Snake
{
public:
    Snake(Pixel *pixel, RestServer *server) : pixel(pixel), server(server) {}

    void reset()
    {
        pixel->setAllPixels(Colors::BLACK);
        for (int i = 0; i < max_length; i++)
        {
            snake[i][0] = -1;
            snake[i][1] = -1;
        }
        snake[0][0] = 0;
        snake[0][1] = 2;
        snake[1][0] = 0;
        snake[1][1] = 1;
        snake[2][0] = 0;
        snake[2][1] = 0;
        length = 3;

        food[0] = 4;
        food[1] = 5;
        draw();
    }

    void play()
    {
        while (true)
        {
            server->handleBackground();
            if (lasttime + updateRate < millis())
            {
                if (server->action != 0)
                {
                    state = server->action;
                }
                switch (state)
                {
                case 0:
                    break;
                case 1: //exit
                    server->action = 0;
                    return;
                default:
                    for (int i = 1; i < length; i++)
                    {
                        snake[length - i][0] = snake[length - i - 1][0];
                        snake[length - i][1] = snake[length - i - 1][1];
                    }
                    break;
                }
                switch (state)
                {
                case 2: // left
                    snake[0][1] -= 1;
                    snake[0][1] %= 11;
                    break;
                case 3: // top
                    snake[0][0] -= 1;
                    snake[0][0] %= 10;
                    break;
                case 4: // right
                    snake[0][1] += 1;
                    snake[0][1] %= 11;
                    break;
                case 5: //down
                    snake[0][0] += 1;
                    snake[0][0] %= 10;
                    break;
                }
                // todo collision
                if (snake[0][0] == food[0] && snake[0][1] == food[1])
                {
                    length += 1;
                    snake[length - 1][0] = snake[length - 2][0];
                    snake[length - 1][1] = snake[length - 2][1];
                    food[0] = random(0, 10);
                    food[1] = random(0, 11);
                }
                server->action = 0;
                draw();
                lasttime = millis();
            }
            delay(3);
        }
    }

private:
    static const int max_length = 30;
    Pixel *pixel;
    RestServer *server;

    int food[2] = {4, 5};

    int snake[max_length][2];
    int length = 3;

    long lasttime = 0L;
    const int updateRate = 150;

    byte state = 0;

    void draw()
    {
        pixel->setAllPixels(Colors::BLACK);
        pixel->setPixel(pixel_matrix[food[0]][food[1]], Colors::GREEN);

        pixel->setPixel(pixel_matrix[snake[0][0]][snake[0][1]], Colors::BLUE);
        for (int i = 1; i < length; i++)
        {
            pixel->setPixel(pixel_matrix[snake[i][0]][snake[i][1]], Colors::WHITE);
        }
        pixel->update();
    }
};
#endif //SNAKE_H