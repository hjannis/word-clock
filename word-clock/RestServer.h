#ifdef ETHERNET_EDITION
#ifndef RESTSERVER_H
#define RESTSERVER_H

#include <EEPROM.h>

#include "Definitions.h"

#include "GenericSmartHomeDevice.h"
#include "Pixel.h"
#include "Clock.h"
#include "Timer.h"
#include <map>

class RestServer {
public:
  byte action = 0;
  int user_brightness = 255;
  boolean manual_refresh_display = false;
  boolean manual_brightness = false;
  long time_set_brightness = 0L;
  RestServer(Pixel *pixel, Clock *clock, Timer *timer)
    : device(SERVER_PORT) {
    this->pixel = pixel;
    this->clock = clock;
    this->timer = timer;
    clock->setConnecting();
    server = device.getServer();
    initRest();
  }

  void begin() {
    device.begin();
    setupOTA();
    restoreAllFromEEPROM();
  }

  void returnOK() {
    returnOK("");
  }

  void returnOK(String s) {
    String log = "ok\n" + s;
#ifdef DEBUG
    Serial.println(log);
#endif
    server->send(200, "text/plain", log);
  }

  void returnError() {
    returnError("");
  }

  void returnError(String s) {
    String log = "error " + s;
#ifdef DEBUG
    Serial.println(log);
#endif
    server->send(200, "text/plain", log);
  }

  void handleBackground() {
    ArduinoOTA.handle();
    device.serverBackgroundJob();
  }

  int getMode() {
    return mode;
  }

private:
  Pixel *pixel;
  Clock *clock;
  Timer *timer;
  GenericSmartHomeDevice device;
  ESP8266WebServer *server;
  int mode = 0;
  int lastmode = 0;

  String draw_index =
    "<!DOCTYPE HTML><html><head>\n"
    "<title>ESP Input Form</title>\n"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
    "</head><body>\n"
    "<form action=\"/pixel\">\n"
    "<form id='board'>"
    "<table>"
    "<?php for($y = 0; $y < 10; $y++): ?>"
    "<tr class=\"row-<?= $y ?>\">"
    "<?php for($x = 0; $x < 10; $x++): ?>"
    "<td class=\"col-<?= $x ?>\"><input type='color' name=\"board[<?= $x ?>][<?= $y ?>]\" data-x=\"<?= $x ?>\" data-y=\"<?= $y ?>\"/>"
    "<?php endfor; ?>"
    "</tr>"
    "<?php endfor; ?> "
    "</table>"
    "</form>"
    "<input type=\"submit\" value=\"submit\" />\n"
    "</form>\n"
    "</body></html>";

  String color_index =
    "<!DOCTYPE HTML><html><head>\n"
    "<title>ESP Input Form</title>\n"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
    "</head><body>\n"
    "<form action=\"/clockcolor\">\n"
    "<label for=\"colorpicker\">Color Picker:</label>"
    "<input type=\"color\" name=\"val\" value=\"#0000ff\">"
    "<input type=\"range\" name=\"whiteval\" min=\"0\" max=\"255\" value=\"#00\">"
    "<input type=\"submit\" value=\"submit\" />\n"
    "</form>\n"
    "</body></html>";

  void storeAllToEEPROM() {
#ifdef DEBUG
    Serial.println("Store: Mode: " + String(mode) + " : Brightness: " + String(user_brightness) + " : Color: " + String(pixel_color, HEX));
#endif
    EEPROM.write(384, mode & 0xff);
    EEPROM.write(385, user_brightness & 0xff);

    EEPROM.write(386, ((pixel_color & 0x00ff0000) >> 16) & 0xff);
    EEPROM.write(387, ((pixel_color & 0x0000ff00) >> 8) & 0xff);
    EEPROM.write(388, ((pixel_color & 0x000000ff)) & 0xff);
#ifndef RGB
    EEPROM.write(389, ((pixel_color & 0xff000000) >> 24) & 0xff);
#endif
    EEPROM.commit();
  }

  void restoreAllFromEEPROM() {
    mode = EEPROM.read(384);
    user_brightness = EEPROM.read(385);
    pixel_color = 0x00000000;

    pixel_color |= (EEPROM.read(386) << 16);
    pixel_color |= (EEPROM.read(387) << 8);
    pixel_color |= EEPROM.read(388);
#ifndef RGB
    pixel_color |= (EEPROM.read(389) << 24);
#endif

#ifdef DEBUG
    Serial.println("Restore: Mode: " + String(mode) + " : Brightness: " + String(user_brightness) + " : Color: " + String(pixel_color, HEX));
#endif
  }

  void setupOTA() {
    ArduinoOTA.setHostname(DEVICE_NAME);
    ArduinoOTA.onStart([&]() {
      pixel->setAllPixels(0x0000ff);
      pixel->update();
    });
    ArduinoOTA.onEnd([&]() {
      pixel->setAllPixels(0x00ff00);
      pixel->update();
      delay(100);
      pixel->setAllPixels(0x000000);
      pixel->update();
    });

    ArduinoOTA.onProgress([&](unsigned int progress, unsigned int total) {
      pixel->getPixels()->setBrightness(255);
      for (int i = 0; i < ((progress * pixel->getLedCount()) / total); i++) {
        pixel->setPixel(i, 0x0000ff);
      }
      pixel->update();
    });
    ArduinoOTA.onError([&](ota_error_t error) {
      pixel->setAllPixels(0xff0000);
      pixel->update();
      delay(100);
      pixel->setAllPixels(0x000000);
      pixel->update();
      delay(100);
      pixel->setAllPixels(0xff0000);
      pixel->update();
    });

    ArduinoOTA.begin();
  }

  void initRest() {
    server->on("/timer", HTTP_GET, [&]() {
      mode = 4;
      timer->setDuration(server->arg("duration").toInt());
      timer->start();
      returnOK();
    });
    server->on("/timer/exit", HTTP_GET, [&]() {
      mode = 0;
      timer->stop();
      manual_refresh_display = true;
      returnOK();
    });

    server->on("/drawpixel", HTTP_GET, [&]() {
      server->send(200, "text/html", draw_index);
    });

    server->on("/pixel", HTTP_GET, [&]() {
      lastmode = 0;
      mode = 0xff;
      int pixel_nb = server->arg("id").toInt();
      uint32_t color = strtol(server->arg("color").c_str(), 0, 16);
      pixel->setPixel(pixel_nb, color);
      manual_refresh_display = true;
      returnOK();
    });
    server->on("/pixel/exit", HTTP_GET, [&]() {
      mode = lastmode;
      lastmode = 0xff;
      manual_refresh_display = true;
      returnOK();
    });
    server->on("/brightness", HTTP_GET, [&]() {
      user_brightness = server->arg("value").toInt();
      manual_refresh_display = true;
      manual_brightness = true;
      time_set_brightness = millis();
      storeAllToEEPROM();
      returnOK();
    });

    server->on("/color", HTTP_GET, [&]() {
      server->send(200, "text/html", color_index);
    });

    server->on("/clockcolor", HTTP_GET, [&]() {
      if (server->hasArg("value")) { pixel_color = strtol(server->arg("value").c_str(), 0, 16); }  //TODO improve app
      if (server->hasArg("val")) {
        pixel_color = (strtol(server->arg("whiteval").substring(1).c_str(), 0, 16) << 24) | strtol(server->arg("val").substring(1).c_str(), 0, 16);
        pixel->setAllPixels(pixel_color);
        pixel->update();
        delay(1000);
      }
      manual_refresh_display = true;
      storeAllToEEPROM();
      returnOK();
    });
    server->on("/timeoffset", HTTP_GET, [&]() { /*for legacy reasons*/
                   returnOK(); });
    server->on("/default", HTTP_GET, [&]() {
      mode = 0;
      manual_refresh_display = true;
      storeAllToEEPROM();
      returnOK();
    });
    server->on("/digiclock", HTTP_GET, [&]() {
      mode = 6;
      manual_refresh_display = true;
      storeAllToEEPROM();
      returnOK();
    });
    server->on("/roundclock", HTTP_GET, [&]() {
      mode = 0;
      manual_refresh_display = true;
      storeAllToEEPROM();
      returnOK();
    });
#ifdef WITH_GYRO
    server->on("/balanceboard", HTTP_GET, [&]() {
      lastmode = 0;
      mode = 5;
      manual_refresh_display = true;
      returnOK();
    });
    server->on("/balanceboard/exit", HTTP_GET, [&]() {
      action = 1;
      mode = 0;
      manual_refresh_display = true;
      returnOK();
    });
#endif

    server->on("/snake", HTTP_GET, [&]() {
      lastmode = 0;
      mode = 3;
      returnOK();
    });
    server->on("/snake/exit", HTTP_GET, [&]() {
      action = 1;
      mode = 0;
      manual_refresh_display = true;
      returnOK();
    });
    server->on("/snake/left", HTTP_GET, [&]() {
      action = 2;
      returnOK();
    });
    server->on("/snake/top", HTTP_GET, [&]() {
      action = 3;
      returnOK();
    });
    server->on("/snake/right", HTTP_GET, [&]() {
      action = 4;
      returnOK();
    });
    server->on("/snake/down", HTTP_GET, [&]() {
      action = 5;
      returnOK();
    });
    server->on("/pong", HTTP_GET, [&]() {
      lastmode = 0;
      mode = 2;
      returnOK();
    });
    server->on("/pong/exit", HTTP_GET, [&]() {
      action = 0b0000001;
      mode = 0;
      manual_refresh_display = true;
      returnOK();
    });
    server->on("/pong/p1/up", HTTP_GET, [&]() {
      action |= 0b00000011;
      returnOK();
    });
    server->on("/pong/p1/down", HTTP_GET, [&]() {
      action |= 0b00000010;
      returnOK();
    });
    server->on("/pong/p2/up", HTTP_GET, [&]() {
      action |= 0b00000101;
      returnOK();
    });
    server->on("/pong/p2/down", HTTP_GET, [&]() {
      action |= 0b00000100;
      returnOK();
    });
  }
};
#endif  // RESTSERVER_H
#endif  // ETHERNET_EDITION