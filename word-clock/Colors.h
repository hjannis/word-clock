#ifndef COLORS_H
#define COLORS_H

#include "Definitions.h"

#ifdef RGB
#define COLOR(r, g, b, w) ((uint32_t)r << 16) | ((uint32_t)g << 8) | b;
#else
#define COLOR(r, g, b, w) ((uint32_t)w << 24) | ((uint32_t)r << 16) | ((uint32_t)g << 8) | b;
#endif

class Colors
{

public:
    static const uint32_t BLACK = COLOR(0, 0, 0, 0);
    static const uint32_t RED = COLOR(255, 0, 0, 0);
    static const uint32_t GREEN = COLOR(0, 255, 0, 0);
    static const uint32_t BLUE = COLOR(0, 0, 255, 0);
    static const uint32_t YELLOW = COLOR(0, 255, 255, 0);
#ifdef RGB
    static const uint32_t WHITE = COLOR(255, 255, 255, 0);
#else
    static const uint32_t WHITE = COLOR(0, 0, 0, 255);
#endif
};
#endif // COLORS_H