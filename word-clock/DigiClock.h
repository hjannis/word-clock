#ifndef DIGI_CLOCK_H
#define DIGI_CLOCK_H
#include "Pixel.h"
#include "Clock.h"
#include "Colors.h"

class DigiCLock
{
public:
    DigiCLock(Pixel *pixel)
    {
        this->pixel = pixel;
    }

    void time(long actualTime)
    {
        int min = numberOfMinutes(actualTime);
        int hour = numberOfHours(actualTime);

        pixel->setAllPixels(Colors::BLACK);

        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                int h1 = number_seg_map[hour / 10][i];
                int h2 = number_seg_map[hour % 10][i];
                int m1 = number_seg_map[min / 10][i];
                int m2 = number_seg_map[min % 10][i];
                if (h1 != -1)
                    pixel->setPixel(segment_map[0][h1][j], pixel_color);
                if (h2 != -1)
                    pixel->setPixel(segment_map[1][h2][j], pixel_color);
                if (m1 != -1)
                    pixel->setPixel(segment_map[2][m1][j], pixel_color);
                if (m2 != -1)
                    pixel->setPixel(segment_map[3][m2][j], pixel_color);
            }
        }
        pixel->update();
    }

private:
    Pixel *pixel;

    int number_seg_map[10][7] = {
        {0, 2, 3, 4, 5, 6, -1},
        {4, 6, -1, -1, -1, -1, -1},
        {0, 4, 1, 5, 2, -1, -1},
        {0, 1, 2, 4, 6, -1, -1},
        {3, 1, 4, 6, -1, -1, -1},
        {0, 1, 2, 3, 6, -1, -1},
        {0, 1, 2, 3, 5, 6, -1},
        {0, 4, 6, -1, -1, -1, -1},
        {0, 1, 2, 3, 4, 5, 6},
        {0, 1, 2, 3, 4, 6, -1}};

    int segment_map[4][7][3] = {
        {// 1. stelle
         {109, 108, 107},
         {87, 86, 85},
         {65, 64, 63},
         {109, 88, 87},
         {107, 90, 85},
         {87, 66, 65},
         {85, 68, 63}},
        {// 2. Stelle
         {105, 104, 103},
         {83, 82, 81},
         {61, 60, 59},
         {105, 92, 83},
         {103, 94, 81},
         {83, 70, 61},
         {81, 72, 59}},
        {// 3. Stelle
         {48, 49, 50},
         {26, 27, 28},
         {4, 5, 6},
         {48, 39, 26},
         {50, 37, 28},
         {26, 17, 4},
         {28, 15, 6}},
        {// 4.Stelle
         {52, 53, 54},
         {30, 31, 32},
         {8, 9, 10},
         {52, 35, 30},
         {54, 33, 32},
         {30, 13, 8},
         {32, 11, 10}}};
};

#endif // DIGI_CLOCK_H
