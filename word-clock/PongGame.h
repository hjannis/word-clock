#ifndef PONG_H
#define PONG_H

#include "Pixel.h"
#include "RestServer.h"
#include "Colors.h"
#include "Util.h"
#include "Test.h"

class PongGame
{
public:
    PongGame(Pixel *pixel, RestServer *server) : pixel(pixel), server(server) {}

    void reset()
    {
        pixel->setAllPixels(Colors::BLACK);
        p1 = 4;
        p2 = 4;
        ball[0] = 4;
        ball[1] = 5;
        s[0] = 0.0f;
        s[1] = 0.03f;
        draw();
    }
    void play()
    {
        while (true)
        {
            server->handleBackground();
            if (lasttime + updateRate < millis())
            {
                switch (server->action)
                {
                case 0b00000000:
                    break;
                case 0b00000001: //exit
                    server->action = 0;
                    return;
                case 0b00000010: //p1 down
                    if (p1 + length < 10)
                        p1 += 1;
                    break;
                case 0b00000011: //p1 up
                    if (p1 > 0)
                        p1 -= 1;
                    break;
                case 0b00000100: //p2 down
                    if (p2 + length < 10)
                        p2 += 1;
                    break;
                case 0b00000101: //p2 up
                    if (p2 > 0)
                        p2 -= 1;
                    break;
                }
                server->action = 0;
                //update ball

                ball[0] += s[0];
                ball[1] += s[1];
                if (ball[0] <= 0.0f || ball[0] >= 9.0f)
                {
                    s[0] *= -1.0f + ((float)random(-100, 100) / 100.0f); // bounce + jittering
                }
                if (ball[1] < 1.0 && abs(ball[0] - p1) < length)
                {
                    s[1] *= -1.0f;                                                                            // bounce
                    s[0] += (abs(ball[0] - p1) - ((length - 1) / 2)) * ((float)random(-100, 100) / 10000.0f); // + jittering
                }
                else if (ball[1] > 10.0f && abs(ball[0] - p1) < length)
                {
                    s[1] *= -1.0f;                                                                            // bounce
                    s[0] += (abs(ball[0] - p2) - ((length - 1) / 2)) * ((float)random(-100, 100) / 10000.0f); // + jittering
                }
                else if (ball[1] <= 0.0f || ball[1] >= 10.0f)
                {
                    //test.circle();
                    delay(3000);
                    // todo countdown
                    reset();
                }
                draw();
                lasttime = millis();
            }
            delay(3);
        }
    }

private:
    Pixel *pixel;
    RestServer *server;
    float ball[2] = {4.0f, 5.0f};
    int p1 = 4;
    int p2 = 4;
    float s[2] = {0.0f, 0.03f};

    long lasttime = 0L;

    const int length = 3;
    const int updateRate = 15;

    void draw()
    {
        pixel->setAllPixels(Colors::BLACK);
        pixel->setPixel(pixel_matrix[std::min(9, (int)ball[0])][std::min(10, (int)ball[1])], Colors::GREEN);

        for (int i = 0; i < length; i++)
        {
            pixel->setPixel(pixel_matrix[p1 + i][0], Colors::WHITE);
            pixel->setPixel(pixel_matrix[p2 + i][10], Colors::WHITE);
        }
        pixel->update();
    }
};
#endif // PONG_H
