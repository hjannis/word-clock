#ifdef WITH_DCF
#ifndef MYDCF77_H
#define MYDCF77_H

#include "Definitions.h"

/**
 * Number of milliseconds to elapse before we assume a "1",
 * if we receive a falling flank before - its a 0.
 */
#define DCF_split_millis 140
/**
 * There is no signal in second 59 - detect the beginning of
 * a new minute.
 */
#define DCF_sync_millis 1200

/**
 * DCF time format struct
 */
struct DCF77Buffer
{
    unsigned long long prefix : 21;
    unsigned long long Min : 7;     // minutes
    unsigned long long P1 : 1;      // parity minutes
    unsigned long long Hour : 6;    // hours
    unsigned long long P2 : 1;      // parity hours
    unsigned long long Day : 6;     // day
    unsigned long long Weekday : 3; // day of week
    unsigned long long Month : 5;   // month
    unsigned long long Year : 8;    // year (5 -> 2005)
    unsigned long long P3 : 1;      // parity
};

struct
{
    unsigned char parity_flag : 1;
    unsigned char parity_min : 1;
    unsigned char parity_hour : 1;
    unsigned char parity_date : 1;
} flags;

class MyDCF77
{
public:
    MyDCF77() {}
    /**
 * Initialize the DCF77 routines: initialize the variables,
 * configure the interrupt behaviour.
 */
    void init()
    {
        previousSignalState = 0;
        previousFlankTime = 0;
        bufferPosition = 0;
        dcf_rx_buffer = 0;
        ss = mm = hh = day = mon = year = 0;
#ifdef DEBUG
        Serial.println("Initializing DCF77 routines");
        Serial.print("Using DCF77 pin #");
        Serial.println(DCF_PIN);
#endif
        pinMode(DCF_PIN, INPUT);
        pinMode(DCF_PIN, INPUT);
#ifdef DEBUG
        Serial.println("Initializing timerinterrupt");
#endif
        //Timer2 Settings: Timer Prescaler /64,
        TCCR2 |= (1 < 59);
        // a new minute begins -> time to call finalizeBuffer().
    }

    int update()
    {
        int0handler();
        if (DCFSignalState != previousSignalState)
        {
            scanSignal();
            previousSignalState = DCFSignalState;
        }
        return bufferPosition;
    }

    long long getUnix()
    {
        return unixzeit(year, mon, day, hh, mm, ss);
    }

    int getYear()
    {
        return year;
    }

    int getMon()
    {
        return mon;
    }

    int getDay()
    {
        return day;
    }

    int getHour()
    {
        return hh;
    }

    int getMin()
    {
        return mm;
    }

    int getSecond()
    {
        return ss;
    }

private:
    int tick_counter = 0;
    int TIMSK;
    int TCCR2;
    int OCIE2;

    /**
 * Clock variables
 */
    volatile unsigned char DCFSignalState = 0;
    unsigned char previousSignalState;
    int previousFlankTime;
    int bufferPosition;
    unsigned long long dcf_rx_buffer;

    /**
 * time vars: the time is stored here!
 */
    volatile unsigned char ss;
    volatile unsigned char mm;
    volatile unsigned char hh;
    volatile unsigned char day;
    volatile unsigned char mon;
    volatile unsigned int year;

    long long unixzeit(int jahr, int monat, int tag,
                       int stunde, int minute, int sekunde)
    {
        if (jahr == 0 && monat == 0 && tag == 0 && stunde == 0 && minute == 0 && sekunde == 0)
            return 0L;
        const short tage_seit_jahresanfang[12] = /* Anzahl der Tage seit Jahresanfang ohne Tage des aktuellen Monats und ohne Schalttag */
            {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};

        int schaltjahre = ((jahr - 1) - 1968) / 4 /* Anzahl der Schaltjahre seit 1970 (ohne das evtl. laufende Schaltjahr) */
                          - ((jahr - 1) - 1900) / 100 + ((jahr - 1) - 1600) / 400;

        long long tage_seit_1970 = (jahr - 1970) * 365 + schaltjahre + tage_seit_jahresanfang[monat - 1] + tag - 1;

        if ((monat > 2) && (jahr % 4 == 0 && (jahr % 100 != 0 || jahr % 400 == 0)))
            tage_seit_1970 += 1; /* +Schalttag, wenn jahr Schaltjahr ist */

        return sekunde + 60 * (minute + 60 * (stunde + 24 * tage_seit_1970));
    }

    /**
 * Evaluates the information stored in the buffer. This is where the DCF77
 * signal is decoded and the internal clock is updated.
 */
    void finalizeBuffer(void)
    {
        if (bufferPosition == 59)
        {
#ifdef DEBUG
            Serial.println("Finalizing Buffer");
#endif
            struct DCF77Buffer *rx_buffer;
            rx_buffer = (struct DCF77Buffer *)(unsigned long long)&dcf_rx_buffer;
            if (flags.parity_min == rx_buffer->P1 &&
                flags.parity_hour == rx_buffer->P2 &&
                flags.parity_date == rx_buffer->P3)
            {
#ifdef DEBUG
                Serial.println("Parity check OK - updating time.");
#endif
                //convert the received bits from BCD
                mm = rx_buffer->Min - ((rx_buffer->Min / 16) * 6);
                hh = rx_buffer->Hour - ((rx_buffer->Hour / 16) * 6);
                day = rx_buffer->Day - ((rx_buffer->Day / 16) * 6);
                mon = rx_buffer->Month - ((rx_buffer->Month / 16) * 6);
                year = 2000 + rx_buffer->Year - ((rx_buffer->Year / 16) * 6);
            }
#ifdef DEBUG
            else
            {
                Serial.println("Parity check NOK - running on internal clock.");
            }
#endif
        }
        // reset stuff
        ss = 0;
        bufferPosition = 0;
        dcf_rx_buffer = 0;
    }

    void appendSignal(unsigned char signal)
    {
#ifdef DEBUG
        Serial.print(", appending value ");
        Serial.print(signal, DEC);
        Serial.print(" at position ");
        Serial.println(bufferPosition);
#endif
        dcf_rx_buffer = dcf_rx_buffer | ((unsigned long long)signal << bufferPosition);
        switch (bufferPosition)
        {
        case 21:
        case 29:
        case 36:
            flags.parity_flag = 0;
            break;
        case 28:
            flags.parity_min = flags.parity_flag;
            break;
        case 35:
            flags.parity_hour = flags.parity_flag;
            break;
        case 58:
            flags.parity_date = flags.parity_flag;
            break;
        default:
            break;
        }
        // When we received a 1, toggle the parity flag
        if (signal == 1)
        {
            flags.parity_flag = flags.parity_flag ^ 1;
        }
        bufferPosition++;
        if (bufferPosition > 59)
        {
            finalizeBuffer();
        }
    }

    /**
 * Evaluates the signal as it is received. Decides whether we received
 * a "1" or a "0" based on the
 */
    void scanSignal(void)
    {
        if (DCFSignalState == 1)
        {
            int thisFlankTime = millis();
            if (thisFlankTime - previousFlankTime > DCF_sync_millis)
            {
#ifdef DEBUG
                Serial.println("####");
                Serial.println("#### Begin of new Minute!!!");
                Serial.println("####");
#endif
                finalizeBuffer();
            }
            previousFlankTime = thisFlankTime;
#ifdef DEBUG
            Serial.print(previousFlankTime);
            Serial.print(": DCF77 Signal detected, ");
#endif
        }
        else
        {
            /* or a falling flank */
            int difference = millis() - previousFlankTime;
#ifdef DEBUG
            Serial.print("duration: ");
            Serial.print(difference);
#endif
            if (difference < DCF_split_millis)
            {
                appendSignal(0);
            }
            else
            {
                appendSignal(1);
            }
        }
    }

    /**
 * Interrupthandler for INT0 - called when the signal on Pin 2 changes.
 */
    void int0handler()
    {
        // check the value again - since it takes some time to
        // activate the interrupt routine, we get a clear signal.
        DCFSignalState = digitalRead(DCF_PIN);
    }
};
#endif // MYDCF77_H
#endif // WITH_DCF