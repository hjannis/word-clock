# Word Clock
Version 0.2

Beim Design der Uhr stand in erste Linie die Funktionalität sowie der einfache, für jeden durchführbare Zusammenbau im Fokus.
Dieses Projekt ist so gedacht, dass es von allen erweitert, umgestaltet und weiter geteilt werden kann.

## Zusammenbau
Der Zusammenbau kann Komponentenweise und weitestgehend unabhängig voneinander durchgeführt werden.
Dies soll der einfachen Montage, einer erweiterten Reparierbarkeit dienen und so letztendlich zu einer langen Lebensdauer der Uhr und viel Freude für deren Bauer führen.

Die für die jeweilige Komponente benötigten Materialen sind zu beginn jedes Abschnitts aufgeführt.
**Alle Maße werden in Millimetern angegeben!**

Im Folgenden wird der Aufbau der Komponenten beschrieben; der finale Zusammenbau aller Komponenten, die Programmierung und die Inbetriebnahmen werden abschließend beschrieben.

### A: Trägerrahmen

#### Material
- 4x 500x20x20mm Holzleiste
- 11x Multiclip (zum Selberdrucken: *multiclip.stl*; empfohlen PLA/PETG, 0.2mm Schichtdicke, 95% size xyz)
- 4x Neodym-Magnete, rund 15x2mm
- 12V-DCBuchse (mind. 3A), **WICHTIG: Nicht länger als 25mm!**
- 4x M4x8 Einschraubgewinde (Außendurchmesser (d) max. 8mm)
- 2x Schlüssellochaufhänger, Linsenkopfbeschlag, länglich, max. 16mm breit + passende Schrauben
- ~10cm Zwei-Adriges Kabel
- JST-Anschluss female + 2x Steckerbuchsen
- Schrumpfschlauch 1-1.5mm
- Holzleim
- Epoxid-Kleber (oder schnell trocknender Zwei-Komponenten-Kleber)

#### Werkzeug
- 16mm Fräskopf, flach
- 4mm Holzbohrer
- 6mm Holzbohrer
- Standbohrmaschine
- Schraubzwingen
- Kappsäge
- Crimpzange Dupont/JST
- Heißluftföhn
- Lötkolben

#### Ablauf
1. Holzleisten auf Gehrung schneiden, sodass sie zu einem *Quadrat!* mit 500x500mm äußerer Kantenlänge zusammengesetzt werden können (Bild 1: L1, L2, L3, L4).
2. L1, L2, L3 & L4 an der Innenseite (kurze Seite (Bild 1: A.1)) jeweils drei 4x5mm tiefe Löcher gleichmäßig verteilt (6cm Abstand zur Schnittkante). An der unteren Leiste kein mittleres Loch bohren, hier wird später die DC-Buchse eingesetzt. **!mittig (1cm Kantenabstand)!**bohren.
3. L1 & L3 an der Rückseite (Bild 1: A.2) jeweils zwei 6x10mm tiefe Löcher bohren (4cm Abstand zur Außenkante) und Einschraubgewinde hineindrehen.
4. L2 & L4 an der Vorderseite (Bild 2: A.3) jeweils zwei 16x4mm tiefe Löcher Fräsen (mit dem Fräskopf + Standbohrmaschine). **WICHTIG: Die Innenseiten müssen absolut gleichmäßig und dürfen keine Spanreste aufweisen!**
5. Jeweils ein Neodym-Magnet mit **wenig** Epoxid-Kleber einkleben.
6. L4 von der Außenseite (Bild 1: A.4) durchbohren, sodass die 12V-DC-Buchse bündig hineinpasst.
7. An die 12V-DC-Buchse an beiden Pole eine Ader des Kabels löten, mit Schrumpfschlauch versehen und anschließend beide Enden des Kabels Crimpen und in den JST-Anschluss klipsen.
8. 12V-DC-Buchse in die Bohrung stecken und von der Innenseite mit Epoxid-Kleber verkleben. Aushärten lassen!
9.  Währenddessen L1, L2 & L3 an den Gehrungsstellen verleimen, sodass ein "n" entsteht.
10.  L4 verleimen, sodass ein Quadrat entsteht.
11.  An L1 & L3 jeweils ein Schlüssellochaufhänger in etwa 50-60mm Abstand zur Oberkante anschrauben. **WICHTIG: Die Aufhängung sollten beide den gleichen Abstand zur Oberkante (L2) aufweisen, da die Uhr ansonsten schief hängt!**
12. Multiclips in die vorgesehen Bohrungen (Schritt 2) stecken, Verkleben ist nicht notwendig, da dies den Austausch erleichtert, sollte einer der CLips abbrechen. Die lange Seite der Clips (TODO Bild) muss dabei zur Vorderseite des Rahmens zeigen.

### B: Frontverkleidung

#### Material
- 500x500mm Platte (beliebiges Material, es wird Acrylglas oder Multiplex-Platte empfohlen). Die Dicke richtet sich nach dem Material, erfahrungsgemäß sind 2-3mm gut geeignet
- Verarbeitungsvorlage *clock_front.dxf*

#### Werkzeug
- CNC-Fräse oder Laser-Schneider mit mind. 500x500mm Arbeitsfläche. Die Wahl und die Dicke des Materials muss passend zu dem gewählten Werkzeug sein!

#### Ablauf
1. Platte gemäß den Anweisungen des Werkzeugs verarbeiten. Hierzu die Verarbeitungsvorlage nutzen.
2. Verarbeitete Frontplatte beidseitig glatt schleifen und nach belieben gestalten. Hier dürfen Sie sich nach belieben kreativ austoben.

### C: Diffusorschicht

#### Material
- 500x500x2mm Acrylglasplatte XT Opal weiß

#### Werkzeug
- Tischkreissäge

#### Ablauf
1. Acrylglasplatte auf die Innenmaße des Rahmens zuschneiden, sodass sie mit wenig Kraftaufwand in den in A gebauten Rahmen in die Multiclips eingehängt werden kann.
2. Kannten ggf. entgraten.
3. Tipp: bei Asymmetrie des Zuschnitts die Seiten des Rahmens und der Platte mit einer Zahl markieren, um so die passenden Seiten zuordnen zu können.

### D: LED-Träger & Elektronik
**Dieser Abschnitt ist hardly WiP!**

Die Uhr kann inzwei Varianten gebaut werden:
1. WLAN + NTP-Zeitsynchronisation
2. DCF-Modul + Synchronisation mit Funkuhr (DCF-Modul muss passend der Region z.B. Frankfurt gewählt werden)


#### Material
- 500x500x3mm PVC-Hartschaumplatte
- D1 mini
- Word-Clock PCB *supplier.pcb*
- 3-Adrige Kabel (~2m)
- Schrumpfschlauch 1-1.5mm
- 110 LEDs-LED-Band (10x11 LEDs) (selbstklebend): WS2812b (RGB) oder SK6812 (RGBW), adressierbar, 3-adrig, 30LEDs/m (~3.3cm Abstand zwischen den LEDs)
- 4x LEDs einzeln, gleiches Modell, wie LED-Band, ~10mm Durchmesser
- 10m 5x5mm Vierkantprofil Moosgummidichtung
- JST-Anschluss + Konnektoren male
- (DCF)

#### Werkzeug
- Lötkolben
- Heißklebepistole + Kleber
- Heißluftföhn
- Crimpzange Dupont/JST

#### Ablauf
TODO maße
1. LEDs kleben
2. Verlöten
3. Minuten-LEDs bohren + einkleben
4. Auf Rückseite in Reihe verlöten (111 - 114), siehe elektrische Zeichnung
5. PCB-JST male verlöten, D1 mini-PCB verlöten, D1 mini-LEDs verlöten, PCB-LEDs verlöten
6. (DCF-Modul-D1-Mini verlöten)
7. JST male, D1 mini, PCB, (DCF) ankleben
8. Software konfigurieren, kompilieren und schreiben:
   1. defines entsprechend der Konfiguration setzen
   2. Build + Upload (in Arduino-SW)

### E: Rückverkleidung

#### Material
- 500x500x3mm PVC-Hartschaumplatte

#### Werkzeug
- Cutter-Messer
- 4mm Holzbohrer

#### Ablauf
1. Aussparungen für die in A.11 am Rahmen angebrachten Schlüssellochaufhänger herausschneiden.
2. An den Stellen der vier in A.3 angebrachten Einschraubgewinde ein 4mm Loch durch die Platte Bohren. Bohrungen ggf. etwas senken.

### Zusammenfügen aller Komponenten

#### Material
- 4x M4x16 Senkschraube
- 4x Neodym-Magnete, rund 15x2mm
- 12V-DC-Netzteil
- Epoxid-Kleber

#### Werkzeug
- Schraubenzieher

#### Ablauf
1. Die LED-Trägerplatte (D) in die Clips der Multiclips in der Mitte des Rahmens (A) einhängen.
2. Den JST-Anschluss der 12V-DC-Buchse des Rahmens (A.5) an der LED-Trägerplatte anschließen.
3. Die Diffusorplatte (C) in die äußeren Clips der Multiclips des Rahmens (A) einhängen.
4. Die Rückverkleidung (E) mit der Rückseite des Rahmens (A) in die Einschraubgewinde (A.3) mit den M4-Schrauben verschrauben. Anmerkung: damit die Uhr repariert, erweitert oder mit neuer Software-Programmiert werden kann, wurde die Rückseite so konzipiert, dass sie leicht und ohne Schaden abgenommen und wieder angebracht werden kann. Achten Sie darauf, dass die Rückverkleidung an keiner Stelle mit dem Rahmen verklebt wird.
5. Die vier Neodym-Magnete auf die in A.5 angebrachten Magnete an der Vorderseite des Rahmen setzen.
6. Auf jeden der Magneten eine **kleine** Menge Epoxid-Kleber geben.
7. Die Frontverkleidung (B) so hierauf setzen, dass sie bündig mit dem Rahmen abschließt. **WICHTIG: Die Frontplatte muss dabei kontakt zu allen vier Magneten haben. Achten Sie darauf, dass die Verklebung vollständig aushärtet, bevor Sie die Magnete lösen oder anderweitig belasten.**
8. Schließen Sie das 12V-DC-Netzteil an die Uhr an.

## Einrichtung der Uhr
Die Uhr benötigt zu Synchronisation Zugriff auf Ihr WLAN. Die Einrichtung erfolgt über eine separate Smartphone-App.
Dieser Schritt ist nur notwendig, sollten Sie eine WLAN-fähige Uhr (ohne DCF-Modul) bauen.

### Variante 1: Einrichtung mittels REST-Schinttstelle

### Variante 2: Verbinden mittels Smartphone-App
1. App installieren
2. Uhr an Strom anschließen
3. Smartphone mit WLAN *MySmartDevice* verbinden
4. App öffnen
5. Plus drücken (unten rechts)
6. WLAN-Zugangsdaten eingeben
7. ~1 min warten, zeigt die Uhr dann nicht die richtige Uhrzeit an, Stecker ziehen und erneut einstecken; anschließend kurz warten.

## TODO
- Bild PCB
- Bild Rahmen
- REST-Schnittstelle beschreiben
- Einrichtung
- LED-Träger ausformulieren