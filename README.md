# Word-Clock

The full arduino-based project of the word clock (inspired by https://qlocktwo.com/). But it offers a lot more of functionality e.g. smarthome integration, smartphone control, the usage of the led screen as a playground for snake, pong, ... . 

## Description
The full arduino-based project of the word clock (inspired by https://qlocktwo.com/). But it offers a lot more of functionality e.g. smarthome integration, smartphone control, the usage of the led screen as a playground for snake, pong, ... . 
Additionally you can connect the clock to your local wifi for having the full experience in controlling the clock with automated jobs via your smartphone.
Therefore in future, I will publish some projects for Automate (https://llamalab.com/automate/doc/index.html) which offers a fantastic and easy integratable interface for controlling everthing you want.


## Structure
In *front*-folder you will find the models for machining the front of your clock with a size of 500x500mm out of any material your milling machine is able to process.
The *world-clock*-folder contains the arduino project wich is the heart of tte whole clock.
The flow-files, which you can use in Automate to control your clock via smartphone you can find in the *automate-flows*-folder.
In the near future, a standalone smart-home control-app will be awailable, which will be 100% compatible with the clock.

## You need ...
The full part-, component- and tool-list is available under https://docs.google.com/spreadsheets/d/13XWR3F3BTKtcVYdpah50wCCpvPsMHoVicqu1r3rTawQ/edit?usp=sharing (in german, only).

## Parts you need
- D1 mini (or any other kind of microcontroller)
- WS2812b LED Strips with 110 LEDs
- WS2812b single LEDs
- TODO

## Machines you need
- cnc milling machine or cnc-laser-cutting machine
- Hot Glue
- TODO

## Instruction
Initially, download the whole project folder. The nexts steps will be explained in hardware- and software-section (see image below).
![Download Project from GitLab](handbook/download%20project.png)
2. Follow the setup tutorial: [How to setup an arduino programming environment](https://www.youtube.com/watch?v=F6jAs4W8nn4&ab_channel=Fabsenet)
4. Open the previously downloaded project into the Arduino-Software(*wordclock/word-clock.ino*-file)
    a. Change pins if necessary in *Definitions.h*
    b. set pre-compile macros for your personal setup and preferences (the default should be ok for you)
5. Set the port to the respective one (e.g. COM13)
6. Click *Upload*:  ![upload](handbook/upload.png)
7. The clock should reboot after uploading sucessfully, the Wifi-credentials should remain untouched so the clock will connect without any additional work (in case you have conected it already to you personal wifi)

## Support and Contact
If you have abny trouble building, installing and what so ever with this project, feel free to contact me on my mail: [jannis.hamborg@lab3.org](mailto:jannis.hamborg@lab3.org).

I am very glad for new ideas, possibilites for further development and so on.

## Ideas and issues
Issues and ideas you can post in the respective gitlab-boards.

## API-Documenation
|URL|Description|
|---|---|
|**Setup + Wifi configuration**||
|IP/|GUI based Wifi configuration|
|IP/config?ssid=SSID&pw=PASSWORD|Wifi configuration|
|**Clock configuration**||
|IP/brightness?value=VALUE|Sets the colck brithness to VALUE (0 to 255)|
|IP/color|GUI based clock color configuration|
|IP/clockcolor?value=VALUE|Sets the clock color to VALUE (RGB hex encoded (e.g. 00FF00 - green))|
|***Clock modes***||
|IP/default|Returns to default word clock mode|
|IP/roundclock|Not implemented yet|
|IP/digiclock|Not implemented yet, has to be set via source code|
|*Timer*||
|IP/timer?duration=DURATION|Starts timer with DURATION ms|
|IP/timer/exit|Stops timer at any time|
|*Pixel drawer*||
|IP/drawpixel|GUI based pixel drawing method. You can set any pixel at any color you want|
|IP/pixel?id=ID&color=COLOR|Sets pixel at position ID to color COLOR (RGB hex encoded)|
|IP/pixel/exit|Exits the draw mode and return to default clock|
|**Games**||
|*Snake*||
|IP/snake|Starts the snake game|
|IP/snake/exit|Ends the snake game, returns back to default clock|
|IP/snake/left|Steers to the left|
|IP/snake/top|Steers to the top|
|IP/snake/right|Steers to the right|
|IP/snake/down|Steers to the bottom|
|*Pong*||
|IP/pong|Starts the pong game|
|IP/pong/exit|Ends the pong game, return back to default clock|
|IP/pong/p1/up|Steers racket of Player 1 upwards|
|IP/pong/p1/down|Steers racket of Player 1 downwards|
|IP/pong/p2/up|Steers racket of Player 2 upwards|
|IP/pong/p2/down|Steers racket of Player 2 downwards|


After successful intialization, the device broadcasts an UDP packet to port 8181 containing ESP chipID for future app connection.

## Known issues

- worldtimeapi.org does not work very well, so I have decided to change to ipgeolocation.io. Therefor you haveto add your own API KEY.


## License
Copyright 2022 Jannis Hamborg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

